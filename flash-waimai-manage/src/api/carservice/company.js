import request from '@/utils/carrequest'

/**
 * N001 创建车队账号
 */

export function save(data) {
  return request({
    url: '/platform/api/v1/manage/companies/',
    method: 'post',
    data: data
  })
}

/**
 * N002 查询车队列表
 */

export function list() {
  return request({
    url: '/platform/api/v1/manage/companies/?page=0&size=30',
    method: 'get'
  })
}

/**
 * N008 查询车队路线列表
 */

export function getCompanyRoutes(companyId) {
  return request({
    url: '/platform/api/v1/manage/companies/' + companyId + '/routes/?page=0&size=30',
    method: 'get'
  })
}

/**
 * N009 查询车队班次列表
 */

export function getCompanyNumbers(companyId) {
  return request({
    url: '/platform/api/v1/manage/companies/' + companyId + '/numbers/?page=0&size=30',
    method: 'get'
  })
}

/**
 * TODO 修改车队信息
 * params : companyId
 * data: data
 * response: '修改成功'
 */

/**
 * TODO 删除车队
 * params : companyId
 * response: '删除成功'
 */

/**
 * TODO 冻结车队
 * params : companyId
 * response: '冻结成功'
 */
