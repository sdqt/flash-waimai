// 班次请求
import request from '@/utils/carrequest'

/**
 * N003 查询车队详情
 */

export function getCompanyDetail(companyId) {
  return request({
    url: '/platform/api/v1/manage/companies/' + companyId,
    method: 'get'
  })
}

/**
 * N006 查询路线详情
 */

export function getRoutesDetail(routeId) {
  return request({
    url: '/platform/api/v1/manage/routes/' + routeId,
    method: 'get'
  })
}

/**
 * N0301 创建班次
 */

export function createNumbers(data) {
  return request({
    url: '/company/api/v1/numbers/',
    method: 'post',
    data: data
  })
}

/**
 * N0302 查询车队班次列表
 */

export function getNumbersList(companyId) {
  return request({
    url: '/company/api/v1/numbers/' + companyId + '&page=0&size=30',
    method: 'get'
  })
}

/**
 * N303 查询班次详情
 */
export function getNumbersDetail(companyId, numberId) {
  return request({
    url: '/company/api/v1/numbers/' + numberId + '?companyId=' + companyId,
    method: 'get'
  })
}

/**
 * N304 发布班次
 */

export function publishNumbers(companyId, numberId) {
  return request({
    url: '/company/api/v1/numbers/' + numberId + '?companyId=' + companyId,
    method: 'get'
  })
}

/**
 * N305 批量发布班次
 */
export function batchPublishNumbers(data) {
  return request({
    url: '/company/api/v1/numbers/services/batchPublish',
    method: 'post',
    data: data
  })
}
