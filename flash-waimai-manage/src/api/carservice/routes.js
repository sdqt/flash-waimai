import request from '@/utils/carrequest'

/**
 * N003 查询车队详情
 * 用于查询车队名称
 */

export function getRoutesBelongs(id) {
  return request({
    url: '/platform/api/v1/manage/companies/' + id,
    method: 'get'
  })
}

/**
 * N004 创建车队路线
 */

export function addRoutes(data) {
  return request({
    url: '/platform/api/v1/manage/routes/',
    method: 'post',
    data: data
  })
}

/**
 * N005 查询路线列表
 */

export function getRoutes(params) {
  return request({
    url: '/platform/api/v1/manage/routes/?page=0&size=30',
    method: 'get',
    params
  })
}

/**
 * N007 冻结车队路线
 */

export function freezeRoutes(id) {
  return request({
    url: '/platform/api/v1/manage/routes/' + id + '/freeze',
    method: 'post'
    // data: data
  })
}

/**
 * N006 查询路线详情
 */

export function getRoutesDetail(routeId) {
  return request({
    url: '/platform/api/v1/manage/routes/' + routeId,
    method: 'get'
  })
}

/**
 * N010 查询路线班次列表
 */

export function getRoutesNumbers(routeId) {
  return request({
    url: '/platform/api/v1/manage/routes/' + routeId + '/numbers/?page=0&size=30',
    method: 'get'
  })
}

/**
 * 添加商铺
 */

export function foodCategory(params) {
  return request({
    url: '/shopping/v2/restaurant/category',
    method: 'get'
  })
}

/**
 * 更新餐馆信息
 */

export function updateResturant(params) {
  return request({
    url: '/shopping/updateshop',
    method: 'post',
    params
  })
}

/**
 * 结算金额
 * @param params
 */
export function check(params) {
  return request({
    url: '/shopping/check',
    method: 'post',
    params
  })
}
/**
 * 审核商铺
 * @param params
 */
export function auditResturant(params) {
  return request({
    url: '/shopping/auditShop',
    method: 'post',
    params
  })
}

/**
 * 停用商铺
 * @param params
 */
export function stopResturant(params) {
  return request({
    url: '/shopping/stopShop',
    method: 'post',
    params
  })
}
/**
 * 删除餐馆
 */

export function deleteResturant(id) {
  return request({
    url: '/shopping/restaurants/' + id,
    method: 'delete'
  })
}

/**
 * 获取餐馆详细信息
 */

export function getResturantDetail(id) {
  return request({
    url: '/shopping/restaurant/' + id,
    method: 'get'
  })
}

/**
 * 获取menu列表
 */

export function getMenu(params) {
  return request({
    url: '/shopping/v2/menu/',
    method: 'get',
    params
  })
}

/**
 * 获取menu详情
 */

export function getMenuById(category_id) {
  return request({
    url: '/shopping/v2/menu/' + category_id,
    method: 'get'
  })
}

/**
 * 获取当前店铺食品种类
 */

export function getCategory(restaurant_id) {
  return request({
    url: '/shopping/getcategory/' + restaurant_id,
    method: 'get'
  })
}

/**
 * 添加食品种类
 */

export function addCategory(params) {
  return request({
    url: '/shopping/addcategory',
    method: 'post',
    params
  })
}
