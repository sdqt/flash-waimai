// 核销请求
import request from '@/utils/carrequest'

/**
 * N201 查询核销路线列表
 */
export function getVerificationRoutesList(userId) {
  return request({
    url: '/verification/api/v1/routes/?userId=' + userId + '&page=0&size=30',
    method: 'get'
  })
}

/**
 * N202 查询核销路线详情
 */
export function getVerificationRoutesDetail(routeId) {
  return request({
    url: '/verification/api/v1/routes/' + routeId,
    method: 'get'
  })
}

/**
 * N203 查询路线核销班次列表
 */
export function getRoutesVerificationNumbersList(routeId) {
  return request({
    url: '/verification/api/v1/routes/' + routeId + '/numbers?page=0&size=30',
    method: 'get'
  })
}

/**
 * N204 查询核销班次列表
 */
export function getVerificationNumbersList(userId) {
  return request({
    url: '/verification/api/v1/numbers/?userId=' + userId + '&page=0&size=30',
    method: 'get'
  })
}

/**
 * N205 查询核销班次详情
 */
export function getVerificationNumbersDetail(numberId, userId) {
  return request({
    url: '/verification/api/v1/numbers/' + numberId + '?userId=' + userId,
    method: 'get'
  })
}

/**
 * N206 查询核销信息
 */
export function getVerificationInfo(orderId) {
  return request({
    url: '/verification/api/v1/services/info?orderId=' + orderId,
    method: 'get'
  })
}

/**
 * N207 核销
 */
export function getVerificationVerify(orderId) {
  return request({
    url: '/verification/api/v1/services/verify?orderId=' + orderId,
    method: 'get'
  })
}
