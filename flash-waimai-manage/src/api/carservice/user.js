// 用户
import request from '@/utils/carrequest'

/**
 * N101 查询有效路线列表
 */
export function getRoutesList() {
  return request({
    url: '/user/api/v1/routes/?&page=0&size=30',
    method: 'get'
  })
}

/**
 * N102 查询路线详情
 */
export function getRoutesDetail(routeId) {
  return request({
    url: '/user/api/v1/routes/' + routeId,
    method: 'get'
  })
}

/**
 * N103 查询可报名班次列表
 */
export function getEnableNumbersList() {
  return request({
    url: '/user/api/v1/numbers/services/listEnterable?&page=0&size=30',
    method: 'get'
  })
}

/**
 * N104 查询班次详情
 */
export function getNumbersDetail(numberId) {
  return request({
    url: '/user/api/v1/numbers/' + numberId,
    method: 'get'
  })
}

/**
 * N105 查询指定班次是否可以报名
 * 每日 18 点（不含）前次日及以后发车班次可以报名，每日 18 点（含）后隔日及以后发车班次可以报名
 */
export function checkNumbersEnable(numberId) {
  return request({
    url: '/user/api/v1/numbers/' + numberId + '/enterable',
    method: 'get'
  })
}

/**
 * N106 查询指定始发终到日期的班次列表
 */
export function getAssignNumbersList(start, end, date) {
  return request({
    url: `/user/api/v1/numbers/?start=${start}&end=${end}&date=${date}&page=0&size=30`,
    method: 'get'
  })
}

/**
 * N107 创建报名订单
 */
export function createOrders(data) {
  return request({
    url: '/user/api/v1/orders/',
    method: 'post',
    data: data
  })
}

/**
 * N110 查询报名订单列表
 */
export function getEnableOrdersList(userId) {
  return request({
    url: `/user/api/v1/orders/?userId=${userId}&page=0&size=30`,
    method: 'get'
  })
}

/**
 * N108 查询是否可以支付
 */
export function checkOrdersPayable(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/payable?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N109 查询支付结果
 */
export function getOrdersPayStatus(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/payStatus?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N111 查询报名订单详情
 */
export function getOrdersDetail(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N112 查询是否可以核销
 */
export function checkOrdersVerify(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/verifyable?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N113 查询核销信息
 */
export function getOrdersVerify(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/verification?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N114 查询是否可以退款
 */
export function checkOrdersRefund(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/refundable?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N115 发起第三方退款
 */
export function pushOrdersRefund(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/refund?userId=${userId}`,
    method: 'get'
  })
}

/**
 * N116 查询退款结果
 */
export function getOrdersRefundResult(orderId, userId) {
  return request({
    url: `/user/api/v1/orders/${orderId}/refundStatus?userId=${userId}`,
    method: 'get'
  })
}
