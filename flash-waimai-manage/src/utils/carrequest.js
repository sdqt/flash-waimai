import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import store from '../store'
import { getToken } from '@/utils/auth'
// axios.defaults.withCredentials=true
// 创建axios实例
const carservice = axios.create({
  // baseURL: 'http://121.46.129.213:32104/penco', // api的base_url
  baseURL: 'https://test01.leafcraft.cc/penco',
  withCredentials: false, // send cookies when cross-domain requests
  timeout: 10000

})

// request拦截器
carservice.interceptors.request.use(
  config => {
    // var token = getToken()
    // if (token) {
    //   config.headers['Authorization'] = token // 让每个请求携带自定义token 请根据实际情况自行修改
    // }
    config.headers.common['Content-Type'] = 'application/json;charset=utf-8'
    // config.data = true
    return config
  },
  error => {
    // Do something with request error
    console.log('error', error) // for debug
    Promise.reject(error)
  }
)

// respone拦截器
carservice.interceptors.response.use(
  response => {
    /**
     * code为非20000是抛错 可结合自己业务进行修改
     */
    const res = response.data
    // console.log(res)
    // if (res.code !== 20000) {
    //   // 50008:非法的token; 50012:其他客户端登录了;  50014:Token 过期了;
    //   if (res.code === 50008 || res.code === 50012 || res.code === 50014) {
    //     return Promise.reject(res.msg)
    //   }
    // } else {
    //   return response.data
    // }
    if (res.content && res.content.length > 0) {
      return res.content
    } else {
      return res
    }
  },
  error => {
    // debug
    if (error.response && error.response.data.errors) {
      Message({
        message: error.response.data.errors[0].defaultMessage,
        type: 'error',
        duration: 5 * 1000
      })
    } else {
      if (error.response && error.response.data.message) {
        Message({
          message: error.response.data.message,
          type: 'error',
          duration: 5 * 1000
        })
      } else {
        Message({
          message: error.message,
          type: 'error',
          duration: 5 * 1000
        })
      }
    }
    return Promise.reject(error)
  }
)

export default carservice
